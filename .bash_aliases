## dotfiles command
alias dotfiles="git --git-dir=${HOME}/.dotfiles.git --work-tree=${HOME}"

## shortcuts
alias c='clear'
alias h='history'
alias j='jobs -l'
alias ..='cd ..'

## common aliases
alias mkdir='mkdir -pv'
alias ll='ls --long'

## aliases for date
alias now='date +"%T"'
alias today='date +"%d-%m-%Y"'

## disk usages
alias df='df -H'
alias du='du -ch'

## improved commands
alias_cmd () {
  if command -v "${2}" > /dev/null 2>&1; then
    alias "${1}"="${2}"
  fi
}

alias_cmd cat bat # https://github.com/sharkdp/bat
alias_cmd grep rg # https://github.com/BurntSushi/ripgrep
alias_cmd ls exa # https://the.exa.website/
alias_cmd top htop # https://hisham.hm/htop/
alias_cmd man tldr # https://tldr.sh/
alias_cmd code codium # https://vscodium.com/
# alias_cmd du duc # https://duc.zevv.nl/

cheat () {
  curl "cht.sh/${1}"
}

## update and upgrade aliases
if command -v "brew" > /dev/null 2>&1; then
  alias update='brew update'
  alias upgrade='brew upgrade'
elif command -v "apt" > /dev/null 2>&1; then
  alias update='apt update'
  alias upgrade='apt upgrade'
elif command -v "nix-env" > /dev/null 2>&1; then
  alias upgrade='nix-env -u'
fi

## copy alias
if command -v "pbcopy" > /dev/null 2>&1; then
  copy () {
    pbcopy < "${1}"
  }
elif command -v "xclip" > /dev/null 2>&1; then
  copy () {
    xclip -sel clip < "${1}"
  }
fi
