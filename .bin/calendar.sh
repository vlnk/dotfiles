#! /bin/sh

WIDTH=${WIDTH:-250}
HEIGHT=${HEIGHT:-200}
#DATEFMT=${DATEFMT:-"+%a %d.%m.%Y %H:%M:%S"}
DATEFMT=${DATEFMT:-"+%a %d/%m/%Y %H:%M:%S"}
SHORTFMT=${SHORTFMT:-"+%H:%M:%S"}

DATE=$(date "$DATEFMT")

case "$BLOCK_BUTTON" in
  1|2|3) 

	# the position of the upper left corner of the popup
	posX=$(($BLOCK_X - $WIDTH / 2))
	posY=$(($BLOCK_Y + 20))

	i3-msg -q `exec khal | rofi \
        -dmenu \
        -location 3 \
        -yoffset 20 \
        -hide-scrollbar \
        -font "FantasqueSansMono Nerd Font 9" \
        -width -60 \
        -p "$DATE"
        > /dev/null`
esac

echo "$LABEL$(date "$DATEFMT")"
echo "$LABEL$(date "$SHORTFMT")"