#!/usr/bin/env python3
#
# Copyright (C) 2016 James Murphy
# Licensed under the GPL version 2 only
#
# A battery indicator blocklet script for i3blocks

import re
from subprocess import check_output

def match_time(battery_status):
  time = battery_status[-1].strip()
  # check if it matches a time
  time = re.match(r"(\d+):(\d+)", time)

  if time:
    time = ":".join(time.groups())
    timeleft = " ({})".format(time)
  else:
    timeleft = ""

  return time

def get_battery_percent(battery_status):
  battery_re = re.compile(r': (?P<key>\w+), (?P<percent>\d+)%$')
  batteries = {}

  for status in battery_status.strip().split('\n'):
    m = battery_re.search(status)
    batteries[m.group('key')] = m.group('percent')

  percents = [int(p) for p in batteries.values()]
  return int(sum(percents) / len(percents))

def is_adapter_plugged(adapter_status):
  is_plugged = False

  for status in adapter_status.strip().split('\n'):
    if 'on-line' in status:
      is_plugged = True
      break

  return is_plugged

def get_battery_symbol(battery_percent, adapter_status):
  if is_adapter_plugged(adapter_status):
    return '\uf1e6'
  elif percent < 20:
    return '\uf128'
  else:
    return '\uf240'

statuses = check_output(['acpi'], universal_newlines=True)
adapters = check_output(['acpi', '-a'], universal_newlines=True)

percent = get_battery_percent(statuses)
symbol = get_battery_symbol(percent, adapters)

print(f'{symbol} {percent}%')
