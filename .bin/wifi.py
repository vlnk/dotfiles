#! /usr/bin/env python3

from os import environ, kill
from re import match
from signal import SIGKILL
from subprocess import check_output, Popen, CalledProcessError

raw = check_output(
    ["nmcli", "device", "show"],
    universal_newlines=True
)

network = {}
interface = None
wifi = None
connected = []

for line in raw.splitlines():
    m = match(r'^(?P<key>\S*):\s*(?P<value>.*)$', line)

    if m is None:
        continue
  
    key, value = m.group('key'), m.group('value')
  
    if 'DEVICE' in key:
        network[value] = {}
        interface = network[value]
    else:
        interface[key] = value

    if 'TYPE' in key and 'wifi' in value:
        wifi = interface

try:
    applet = check_output(['pgrep', '-f', 'nm-applet'])
    pid = int(applet.decode('utf8').strip())
    kill(pid, SIGKILL)
except Exception:
    Popen(['nm-applet', '&'])


if not wifi or not 'connected' in wifi['GENERAL.STATE']:
    print('nope')
else:
    print(wifi['GENERAL.CONNECTION'])
