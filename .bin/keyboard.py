#!/home/vlnk/.nix-profile/bin/python

from re import search
from subprocess import check_output

layout_info = check_output(
  ['setxkbmap', '-query'],
  universal_newlines=True
)

layout_states = check_output(
  ['xset', 'q'],
  universal_newlines=True
)

lang = search(r'layout:\s*(\w*)', layout_info).group(1)
capslock_state = search(r'Caps Lock:\s*(\w*)', layout_states).group(1)

if capslock_state == 'on':
  print(lang.upper())
else:
  print(lang)
