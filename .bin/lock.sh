#! /bin/sh

ICON="$HOME/.bin/lock.png"
TMP_BG='/tmp/screen.png'

(( $# )) && { icon=$1; }

scrot "$TMP_BG"
convert "$TMP_BG" -scale 10% -scale 1000% "$TMP_BG"
convert "$TMP_BG" "$ICON" -gravity center -composite -matte "$TMP_BG"
i3lock -u -i "$TMP_BG"