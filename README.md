## Bring me my config !

```bash
GIT_URL="git@gitlab.com:vlnk/dotfiles.git"
git clone --bare "${GIT_URL}" "${HOME}/.dotfiles.git"
```

## Save my configuration !
```bash
# Never ever
alias dotfiles="git --git-dir=${HOME}/.dotfiles/ --work-tree=${HOME}"

dotfiles status
dotfiles add $FILES_YOU_CHANGE
dotfiles commit
dotfiles push
```

## Branch me up !
```bash
dotfiles branch home # work, server
```

## TODO
- Add i3 locks configuration
- Add calandar mode for rofi
- Configure theme for rofi
- Deal with wall / pywall
- Add shutdown menu
- Refine and document blocklets