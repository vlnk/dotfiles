## https://nixos.org/nix/
if [ -e "${HOME}/.nix-profile/etc/profile.d/nix.sh" ]; then
  source "${HOME}/.nix-profile/etc/profile.d/nix.sh"
fi

## https://starship.rs/
if command -v "brew" > /dev/null 2>&1; then
  eval "$(starship init bash)"
fi

## `brew doctor` tell me to do that
if command -v "brew" > /dev/null 2>&1; then
  export PATH="/usr/local/sbin:$PATH"
fi

## https://sw.kovidgoyal.net/kitty/
if command -v "kitty" > /dev/null 2>&1; then
  export TERMINAL=kitty
fi

## https://github.com/dylanaraps/pywal
if command -v "wal" > /dev/null 2>&1; then
  # (cat "${HOME}/.cache/wal/sequences" &) # import colorscheme asynchronously
  source "${HOME}/.cache/wal/colors-tty.sh" # add support for TTYs
fi
